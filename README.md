# QSFExplorer

A Qt5 based GUI application for viewing the contents of SF2 files. It allows listening to individual presets and comparing two files with each other.

Thanks to `fluidsynth` it is possible to connect to an external keyboard via `JACK` audio server interface.

## Build
Prerequisites:
- `cmake` >=3.8
- `clang/g++` with C++17 support
- `Qt5` (tested with 5.15 version)
- `fluidsynth`
